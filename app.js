const Discord = require("discord.js");
const client = new Discord.Client();
const config = require('./config.json');

client.on("ready", () => {
    console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels.`);
    client.user.setActivity(config.status);
});

// Create an event listener for new guild members
client.on('guildMemberAdd', member => {
  // Send the message to a designated channel on a server:
  const channel = member.guild.channels.find(ch => ch.name === 'general');
  // Do nothing if the channel wasn't found on this server
  if (!channel) return;
  // Send the message, mentioning the member
  channel.send(`Welcome to ${member.guild.name}, ${member} - Please remember to sign up at https://forum.plasmagc.com/ - You're member #${member.guild.memberCount}!`);
});

// BOT COMMANDS

// Autoresponder

// Feedback
client.on('message', message => {
  if (message.content.includes('complain') || message.content.includes('feedback')) {
    if (message.author.bot) return;
    message.channel.send(message.author.toString() + ', If you would like to give feedback on Plasma Gaming, please send us a ticket at http://support.plasmagc.com/index.php?a=add&catid=13');
  }
});

// Link
client.on('message', message => {
  if (message.content.includes('link')) {
    if (message.author.bot) return;
    message.channel.send(message.author.toString() + ', For help on linking your account to Plasma Gaming, please follow this guide! https://support.plasmagc.com/knowledgebase.php?article=15 - If you have any issues please submit a Support Ticket!');
  }
});

// Plasma Gaming Rule Updater
client.on('message', async message => {
  if (!message.content.startsWith(config.prefix) || message.author.bot) return;

  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  if (command === 'updaterules') {
    if(message.member.roles.some(r=>["Moderation Manager", "Community Manager", "Developer"].includes(r.name))) { // Is the User a MTL +?
      if (!args.length) { // Did the user not put anything after the command? If so, return an error
          return message.channel.send(`You haven't provided any rules!, ${message.author}!`);
      }
      const sayMessage = args.join(" "); // Replace commas from args with spaces
      const rulechannel = message.guild.channels.find(ch => ch.name === 'rules'); // Always send to rules
      let messagecount = parseInt(100);
      rulechannel.fetchMessages({limit: messagecount}).then(messages => rulechannel.bulkDelete(messages));
      rulechannel.send({
        file: "https://cdn.discordapp.com/attachments/476867249941250067/538758002937954305/discord-rules.png" // Or replace with FileOptions object
      });
      setTimeout(function(){ 
        rulechannel.send(`▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬`);
        rulechannel.send(`\u200B`);
        rulechannel.send(`${sayMessage}`);
        rulechannel.send(`\u200B`);
        rulechannel.send(`▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬`);
        rulechannel.send(`\u200B`);
        rulechannel.send(`Moderators have discretion to act when it is felt an action is detrimental to the well being of the server and its members`);
        rulechannel.send(`Ban Appeals for Discord must be made in the same way through the Forums here: https://forum.plasmagc.com/pages/enforcement/`);
      }, 2000);
    } else {
      message.channel.send(`Insufficient Permissions.`);
      console.log(`${message.author} tried to update the rules, but wasn't authourised to do so!`);
      message.delete(100);
    };
  };
});
// END Plasma Gaming Rule Updater


client.login(config.token);